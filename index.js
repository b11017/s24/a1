

// >> In the S24 folder, create an activity folder and an index.html and index.js file inside of it.

// >> Link the script.js file to the index.html file.

// >> Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

let getCube  = 2 ** 3;
let string1 = "The";
let string2 = "cube";
let string3 = "of";
let string4 = "2";
let string5 = "is";


// >> Using Template Literals, print out the value of the getCube variable with a message:
// 		 The cube of <num> is…

let sentence = `${string1} ${string2} ${string3} ${string4} ${string5} ${getCube}`
console.log(sentence);

// >> Destructure the given array and print out a message with the full address using Template Literals.
	
const address = [" 258 ", " Washington Ave NW ", " California ", " 90011 "];
console.log(`I live at ${address}`)

// 		message: I live at <details>

// >> Destructure the given object and print out a message with the details of the animal using Template Literals.
	
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
};
		

	

// 		message: <name> was a <species>. He weighed at <weight> with a measurement of <measurement>.

console.log(`${animal.name} was a ${animal.species}. He weighed at ${animal.weight} with a measurement of ${animal.measurement}.`);


// >> Loop through the given array of characters using forEach, an arrow function and using the implicit return statement to print out each character
		
const characters = ['Ironman', 'Black Panther', 'Dr. Strange', 'Shang-Chi', 'Falcon']

characters.forEach(characters => console.log(`${characters}`));

// >> Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.

// >> Create/instantiate a 2 new object from the class Dog and console log the object

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let myDog = new Dog();

	myDog.name = 'Doggie';
	myDog.age = '2';
	myDog.breed = 'Aspin';
console.log(myDog);

const newDog = new Dog("Cookie", "2", "Aspin");
	console.log(myDog);

